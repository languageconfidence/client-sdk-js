import uglify from 'rollup-plugin-uglify-es';

export default {
    input: 'src/index.js',
    output: {
        file: 'dist/bundle.js',
        name: 'languageConfidence',
        format: 'esm', // immediately-invoked function expression — suitable for <script> tags
        sourcemap: true
    },
    plugins: [
        uglify()
    ]
};