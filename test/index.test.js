import { LanguageConfidence } from "../src/index.js";
import data_req from "./data/audio1-wav.js";
import data_resp from "./data/response1.js";

global.fetch = require("jest-fetch-mock");

test("Constructs a new LanguageConfidence instance with defaults", () => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  expect(lc.apiKey).toBe("apiKey");
  expect(lc.serverUrl).toBe(
    "https://api.languageconfidence.com/pronunciation-trial"
  );
  expect(lc.difficulty).toBe("beginner");
  expect(lc.phonemeSystem).toBe("ipa");
});

test("Constructs a new LanguageConfidence instance with values", () => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey",
    serverUrl: "server-url",
    difficulty: "beginner",
    customDifficulty: [0.5, 0.6],
    phonemeSystem: "cmu",
    accessToken: "123456789"
  });

  const expected = [0.5, 0.6];

  expect(lc.apiKey).toBe("apiKey");
  expect(lc.serverUrl).toBe("server-url");
  expect(lc.difficulty).toBe("beginner");
  expect(lc.customDifficulty).toEqual(expect.arrayContaining(expected));
  expect(lc.phonemeSystem).toBe("cmu");
  expect(lc.accessToken).toBe("123456789");
});

test("Test sentence score", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let sentenceScore = r.getSentenceScore();
    expect(sentenceScore.label).toBe("AFTER THE SHOW WE WENT TO A RESTAURANT");
    expect(sentenceScore.rawScore).toBe(0.554225);
    expect(sentenceScore.gradedScore).toBe("good");

    done();
  });
});

test("Test sentence score with custom difficulty", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey",
    customDifficulty: [0.5, 0.6]
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let sentenceScore = r.getSentenceScore();
    expect(sentenceScore.label).toBe("AFTER THE SHOW WE WENT TO A RESTAURANT");
    expect(sentenceScore.rawScore).toBe(0.554225);
    expect(sentenceScore.gradedScore).toBe("average");

    done();
  });
});

test("Test word scores list", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let wordScores = r.getWordScoresList();
    expect(wordScores.length).toBe(11);

    let wordScore = wordScores[1];
    expect(wordScore.label).toBe("AFTER");
    expect(wordScore.rawScore).toBe(0.47555);
    expect(wordScore.gradedScore).toBe("good");

    wordScore = wordScores[9];
    expect(wordScore.label).toBe("RESTAURANT");
    expect(wordScore.rawScore).toBe(0.21);
    expect(wordScore.gradedScore).toBe("good");

    done();
  });
});

test("Test individual word scores", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let wordScores = r.getWordScores("AFTER");
    expect(wordScores.length).toBe(1);

    let wordScore = wordScores[0];
    expect(wordScore.label).toBe("AFTER");
    expect(wordScore.rawScore).toBe(0.47555);
    expect(wordScore.gradedScore).toBe("good");

    wordScores = r.getWordScores("RESTAURANT");
    expect(wordScores.length).toBe(1);

    wordScore = wordScores[0];
    expect(wordScore.label).toBe("RESTAURANT");
    expect(wordScore.rawScore).toBe(0.21);
    expect(wordScore.gradedScore).toBe("good");

    done();
  });
});

test("Test phoneme scores list for a word", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let phonemeScores = r.getPhonemeScoresList("RESTAURANT");
    expect(phonemeScores.length).toBe(1); // word restaurant only appeared once
    expect(phonemeScores[0].length).toBe(8); // number of phonemes in the word restaurant

    let phonemeScore = phonemeScores[0][0];
    expect(phonemeScore.label).toBe("ɹ");
    expect(phonemeScore.rawScore).toBe(0.026);
    expect(phonemeScore.gradedScore).toBe("poor");

    phonemeScore = phonemeScores[0][4];
    expect(phonemeScore.label).toBe("ɝ");
    expect(phonemeScore.rawScore).toBe(0.3951);
    expect(phonemeScore.gradedScore).toBe("good");

    phonemeScore = phonemeScores[0][7];
    expect(phonemeScore.label).toBe("t");
    expect(phonemeScore.rawScore).toBe(0.0264);
    expect(phonemeScore.gradedScore).toBe("poor");

    done();
  });
});

test("Test individual phoneme scores", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let phonemeScores = r.getPhonemeScores("t");
    expect(phonemeScores.length).toBe(5); // word restaurant only appeared once

    let phonemeScore = phonemeScores[0];
    expect(phonemeScore.label).toBe("t");
    expect(phonemeScore.rawScore).toBe(0.0745);
    expect(phonemeScore.gradedScore).toBe("poor");

    phonemeScore = phonemeScores[2];
    expect(phonemeScore.label).toBe("t");
    expect(phonemeScore.rawScore).toBe(0.962);
    expect(phonemeScore.gradedScore).toBe("good");

    phonemeScore = phonemeScores[4];
    expect(phonemeScore.label).toBe("t");
    expect(phonemeScore.rawScore).toBe(0.0264);
    expect(phonemeScore.gradedScore).toBe("poor");

    done();
  });
});

test("Test soundsLike for phoneme scores list for a word", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let phonemeScores = r.getPhonemeScoresList("RESTAURANT");
    let phonemeScore = phonemeScores[0][0];
    expect(phonemeScore.soundedLike.length).toBe(5);

    // ["w", 0.864], ["ɛ", 0.0613], ["ɹ", 0.026], ["ɡ", 0.0099], ["b", 0.0087]

    expect(phonemeScore.soundedLike[0][0]).toBe("w");
    expect(phonemeScore.soundedLike[0][1]).toBe(0.864);

    expect(phonemeScore.soundedLike[2][0]).toBe("ɹ");
    expect(phonemeScore.soundedLike[2][1]).toBe(0.026);

    expect(phonemeScore.soundedLike[4][0]).toBe("b");
    expect(phonemeScore.soundedLike[4][1]).toBe(0.0087);

    done();
  });
});

test("Test soundsLike for individual phoneme scores", done => {
  let lc = new LanguageConfidence({
    apiKey: "apiKey"
  });

  fetch.mockResponse(JSON.stringify(data_resp));

  lc.getPronunciationScore(data_req).then(r => {
    let phonemeScores = r.getPhonemeScores("t");
    let phonemeScore = phonemeScores[0];
    expect(phonemeScore.soundedLike.length).toBe(5);

    // [["f", 0.3574], ["θ", 0.2139], ["ɹ", 0.1278], ["ɝ", 0.126], ["t", 0.0745]]

    expect(phonemeScore.soundedLike[0][0]).toBe("f");
    expect(phonemeScore.soundedLike[0][1]).toBe(0.3574);

    expect(phonemeScore.soundedLike[2][0]).toBe("ɹ");
    expect(phonemeScore.soundedLike[2][1]).toBe(0.1278);

    expect(phonemeScore.soundedLike[4][0]).toBe("t");
    expect(phonemeScore.soundedLike[4][1]).toBe(0.0745);

    done();
  });
});
